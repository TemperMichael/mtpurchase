import XCTest
@testable import MTPurchase

final class MTPurchaseTests: XCTestCase {

    // Swift Packages cannot include StoreKit Configurations to test In-App purchases locally, therefore, all purchase related methods need to be tested in an app's project.
}
