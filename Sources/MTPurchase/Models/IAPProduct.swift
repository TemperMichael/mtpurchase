//
//  IAPProduct.swift
//  MTPurchase
//
//  Created by Michael Temper on 03.11.22.
//

import Foundation

public struct IAPProduct: Identifiable {
    public let id: String
    public let title: String
    public let description: String?
    public let price: Decimal?
    public let priceFormatted: String?
    public let imageName: String?
    public let productId: String?
    
    public init(id: String = UUID().uuidString,
                title: String,
                description: String? = nil,
                price: Decimal? = nil,
                priceFormatted: String? = nil,
                imageName: String? = nil,
                productId: String? = nil) {
        self.id = id
        self.title = title
        self.description = description
        self.price = price
        self.priceFormatted = priceFormatted
        self.imageName = imageName
        self.productId = productId
    }
}
