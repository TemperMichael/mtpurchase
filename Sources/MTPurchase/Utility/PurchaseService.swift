//
//  PurchaseService.swift
//  MTPurchase
//
//  Created by Michael Temper on 03.11.22.
//

import Foundation

public protocol PurchaseService {
    
    /**
     Override this method to load your local defined In-App purchase products.
     - returns: Array of local defined In-App purchase products.

     # Example #
     ```
     override func loadLocalProducts() -> [IAPProduct] {
         super.loadLocalProducts()
         return IAPProduct.mock
     }
     ```
     */
    func loadLocalProducts() -> [IAPProduct]
    
    /**
     Loads remote defined In-App purchase products.
     - returns: Array of remote defined In-App purchase products.
     */
    func loadRemoteProducts() async throws -> [IAPProduct]
    
    /**
     Purchase an In-App product with a given product id.
     - parameter productId: The id of the product you want to purchase.
     */
    func purchase(productId: String) async throws
    
    /**
     Restores In-App purchases.
     - returns: A dictionary of product ids with a given boolean value if the purchase was already done.
     */
    func restorePurchases() async throws -> [String: Bool]
}

public class MockPurchaseService: PurchaseService {

    public init() {}

    public func loadLocalProducts() -> [IAPProduct] {
        return []
    }

    public func loadRemoteProducts() async throws -> [IAPProduct] {
        return IAPProduct.mockedCollection
    }

    public func purchase(productId: String) async throws {
        throw InAppPurchaseError.productNotAvailable
    }

    public func restorePurchases() async throws -> [String : Bool] {
        return [:]
    }
}

extension IAPProduct {

    static var mockedCollection: [IAPProduct] = [
        IAPProduct(title: "First Product",
                   description: "This is the product description of the first product",
                   priceFormatted: "1.99",
                   imageName: "MockImage",
                   productId: "ABC"),
        IAPProduct(title: "Second Product",
                   description: "This is the product description of the second product",
                   priceFormatted: "4.99",
                   imageName: "MockImage",
                   productId: "DEF")
    ]
}
