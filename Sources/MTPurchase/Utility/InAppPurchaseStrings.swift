//
//  InAppPurchaseStrings.swift
//  
//
//  Created by Michael Temper on 10.12.22.
//

import Foundation

public struct InAppPurchaseStrings {

    public struct Error {

        public static let generalTitle = String(localized: "iap.error.general.title",
                                                bundle: .module,
                                                comment: "A general error title for errors about In-App purchases")

        public static let generalMessage = String(localized: "iap.error.general.message",
                                                  bundle: .module,
                                                  comment: "A general error message for errors about In-App purchases")
    }

    public struct General {

        public static let restoreAction = String(localized: "iap.general.action.restore.title",
                                                 bundle: .module,
                                                 comment: "The description for the action of restoring In-App purchases")

        public static let purchaseHint = String(localized: "iap.general.purchaseHint.description",
                                                bundle: .module,
                                                comment: "An action hint how to purchase an In-App purchase")
    }

    public struct Accessibility {

        public static let restoreAction = String(localized: "iap.accessibility.action.restore.title",
                                                 bundle: .module,
                                                 comment: "The accesibility description for the action of restoring In-App purchases")
    }
}
