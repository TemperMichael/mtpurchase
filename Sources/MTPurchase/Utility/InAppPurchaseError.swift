//
//  InAppPurchaseError.swift
//  MTPurchase
//
//  Created by Michael Temper on 03.11.22.
//

import Foundation

public enum InAppPurchaseError: Error {
    case userCancelled
    case productNotAvailable
    case canNotMakePayments

}

extension InAppPurchaseError: LocalizedError {
    
    public var errorDescription: String? {
        switch self {
        case .userCancelled:
            return String(localized: "iap.error.usercancelled.description",
                          bundle: .module,
                          comment: "The error message when a user cancels an In-App purchase")
        case .productNotAvailable:
            return String(localized: "iap.error.productNotAvailable.description",
                          bundle: .module,
                          comment: "The error message when a product was not found")
        case .canNotMakePayments:
            return String(localized: "iap.error.cannotmakepayments.description",
                          bundle: .module,
                          comment: "The error message when a product was not found")
        }
    }
}
