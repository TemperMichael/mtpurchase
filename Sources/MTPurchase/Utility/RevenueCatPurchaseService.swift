//
//  RevenueCatPurchaseService.swift
//  MTPurchase
//
//  Created by Michael Temper on 03.11.22.
//
import RevenueCat
import Foundation

public protocol PurchaseDelegate: AnyObject {
    func purchases(readyForPromotedProduct product: IAPProduct)
}

open class RevenueCatPurchaseService: NSObject, PurchaseService {

    public weak var delegate: PurchaseDelegate?

    public init(withAPIKey apiKey: String, logLevel: LogLevel = .debug) {
        // Initialize RevenueCat
        Purchases.logLevel = logLevel
        Purchases.configure(with: .init(withAPIKey: apiKey).with(usesStoreKit2IfAvailable: true))

        super.init()

        Purchases.shared.delegate = self
    }
    
    /**
     Override this method to load your local defined In-App purchase products.
     - returns: Array of local defined In-App purchase products.

     # Example #
     ```
     override func loadLocalProducts() -> [IAPProduct] {
         super.loadLocalProducts()
         return IAPProduct.mock
     }
     ```
     */
    @discardableResult
    open func loadLocalProducts() -> [IAPProduct] {
        return []
    }
    
    /**
     Loads remote defined In-App purchase products on RevenueCat by the current set public API key.
     - returns: Array of remote defined In-App purchase products.
     - warning: You have to set a valid public API key of your current RevenueCat project before calling this method.
     */
    public func loadRemoteProducts() async throws -> [IAPProduct] {
        return try await Purchases.shared.offerings().current?.availablePackages.compactMap {
            IAPProduct(title: $0.storeProduct.localizedTitle,
                       description: $0.storeProduct.localizedDescription,
                       price: $0.storeProduct.price,
                       priceFormatted: $0.storeProduct.localizedPriceString,
                       productId: $0.storeProduct.productIdentifier)
        } ?? []
    }
    
    /**
     Purchase an In-App product via ReveneCat with a given product id.
     - parameter productId: The id of the product you want to purchase.
     */
    public func purchase(productId: String) async throws {
        let products = await Purchases.shared.products([productId])
        
        guard let product = products.first else {
            throw InAppPurchaseError.productNotAvailable
        }
        
        let purchaseResultData = try await Purchases.shared.purchase(product: product)
        
        if purchaseResultData.userCancelled {
            throw InAppPurchaseError.userCancelled
        }
    }
    
    /**
     Restores In-App purchases via RevenueCat.
     - returns: A dictionary of product ids with a given boolean value if the purchase was already done.
     */
    public func restorePurchases() async throws -> [String: Bool] {
        let customerInfo = try await Purchases.shared.restorePurchases()
        let userPurchases = customerInfo.allPurchasedProductIdentifiers.reduce(into: [String: Bool]()) {
            $0[$1] = true
        }
        return userPurchases
    }
}

extension RevenueCatPurchaseService: PurchasesDelegate {

    public func purchases(_ purchases: Purchases, readyForPromotedProduct product: StoreProduct, purchase startPurchase: @escaping StartPurchaseBlock) {
        let product = IAPProduct(title: product.localizedTitle,
                                 description: product.localizedDescription,
                                 price: product.price,
                                 priceFormatted: product.localizedPriceString,
                                 productId: product.productIdentifier)
        delegate?.purchases(readyForPromotedProduct: product)
    }
}
